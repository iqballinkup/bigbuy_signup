import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:signin_signup/pages/register_page.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  int _selectedRadioButton = 0;
  bool _obscureText = true;
  bool isRemembered = false;
  var emailPhoneText = "Phone";
  double textFormFieldHeight = 45.0;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  TextEditingController emailPhoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xffD4EAFF),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    'Sign In',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: screenWidth / 15,
                        color: Colors.blue,
                      ),
                    ),
                  ),
                ),
                // Row(
                //   children: [
                //     Flexible(
                //       child: RadioListTile(
                //         title: Text(
                //           'Bangladesh',
                //           style: GoogleFonts.poppins(
                //             textStyle: TextStyle(
                //               fontWeight: FontWeight.bold,
                //               fontSize: screenWidth / 35,
                //               color: Colors.black54,
                //             ),
                //           ),
                //         ),
                //         value: "bangladesh",
                //         groupValue: nation,
                //         onChanged: (value) {
                //           setState(() {
                //             nation = value.toString();
                //             emailPhoneText = "Phone";
                //           });
                //         },
                //       ),
                //     ),
                //     Flexible(
                //       child: RadioListTile(
                //         title: Text(
                //           'International',
                //           style: GoogleFonts.poppins(
                //             textStyle: TextStyle(
                //               fontWeight: FontWeight.bold,
                //               fontSize: screenWidth / 35,
                //               color: Colors.black54,
                //             ),
                //           ),
                //         ),
                //         value: "international",
                //         groupValue: nation,
                //         onChanged: (value) {
                //           setState(() {
                //             nation = value.toString();
                //             emailPhoneText = "Email";
                //           });
                //         },
                //       ),
                //     ),
                //   ],
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Radio(
                      value: 0,
                      groupValue: _selectedRadioButton,
                      onChanged: (value) {
                        setState(() {
                          _selectedRadioButton = value!;
                          emailPhoneText = "Phone";
                        });
                      },
                    ),
                    Text(
                      'Bangladesh',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: screenWidth / 24,
                          color: Colors.black54,
                        ),
                      ),
                    ),
                    Radio(
                      value: 1,
                      groupValue: _selectedRadioButton,
                      onChanged: (value) {
                        setState(() {
                          _selectedRadioButton = value!;
                          emailPhoneText = "Email";
                        });
                      },
                    ),
                    Text(
                      'International',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: screenWidth / 24,
                          color: Colors.black54,
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  emailPhoneText,
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: textFormFieldHeight,
                  child: TextFormField(
                    controller: emailPhoneController,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }

                      return null;
                    },
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder()),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Password',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: textFormFieldHeight,
                  child: TextFormField(
                    controller: passwordController,
                    obscureText: _obscureText,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            _toggle();
                          },
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Forgot Your Password?',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                Row(
                  children: [
                    Checkbox(
                      checkColor: Colors.white,
                      activeColor: Colors.black,
                      value: isRemembered,
                      onChanged: (bool? value) {
                        setState(() {
                          isRemembered = value!;
                        });
                      },
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      'Remember Me',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontSize: screenWidth / 24,
                          color: Colors.black54,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'LOG IN',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: screenWidth / 22,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Icon(
                        Icons.forward,
                        color: Colors.white,
                      )
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                      minimumSize: const Size.fromHeight(40)),
                ),
                SizedBox(
                  height: 20,
                ),
                Divider(
                  thickness: 2,
                  color: Colors.white,
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text(
                    'or sign in with',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontSize: screenWidth / 22,
                        color: Colors.black54,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        height: 28,
                        width: 28,
                        "images/google.png",
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Login With Google',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 25,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white,
                      minimumSize: const Size.fromHeight(40)),
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "images/facebook.png",
                        height: 28,
                        width: 28,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Login With Facebook',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 25,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white,
                      minimumSize: const Size.fromHeight(40)),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Don\'t have an account?'),
                    SizedBox(
                      width: 5,
                    ),
                    GestureDetector(
                      onTap: (() {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RegisterPage()),
                            (route) => false);
                      }),
                      child: Text(
                        'Register',
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: screenWidth / 24,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
