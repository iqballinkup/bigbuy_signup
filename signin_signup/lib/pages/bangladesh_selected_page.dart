import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:signin_signup/pages/signin_page.dart';

class BangladeshSelectedPage extends StatefulWidget {
  const BangladeshSelectedPage({super.key});

  @override
  State<BangladeshSelectedPage> createState() => _BangladeshSelectedPageState();
}

class _BangladeshSelectedPageState extends State<BangladeshSelectedPage> {
  bool _obscureText1 = true;
  bool _obscureText2 = true;

  bool isRemembered = false;
  double textFormFieldHeight = 45.0;

  void _toggle1() {
    setState(() {
      _obscureText1 = !_obscureText1;
    });
  }

  void _toggle2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }

  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xffD4EAFF),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Your Name *",
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: textFormFieldHeight,
                  child: TextFormField(
                    controller: nameController,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }

                      return null;
                    },
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder()),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Your Phone *",
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: textFormFieldHeight,
                  child: TextFormField(
                    controller: phoneController,
                    keyboardType: TextInputType.phone,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }

                      return null;
                    },
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder()),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Password *',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: textFormFieldHeight,
                  child: TextFormField(
                    controller: passwordController,
                    obscureText: _obscureText1,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText1
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            _toggle1();
                          },
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Confirm Password *',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: textFormFieldHeight,
                  child: TextFormField(
                    controller: confirmPasswordController,
                    obscureText: _obscureText2,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText2
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            _toggle2();
                          },
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Checkbox(
                      checkColor: Colors.white,
                      activeColor: Colors.black,
                      value: isRemembered,
                      onChanged: (bool? value) {
                        setState(() {
                          isRemembered = value!;
                        });
                      },
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Row(
                      children: [
                        Text(
                          'I agree to the ',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidth / 24,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                        Text(
                          'privacy policy',
                          style: GoogleFonts.poppins(
                            decoration: TextDecoration.underline,
                            textStyle: TextStyle(
                              fontSize: screenWidth / 24,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                        Text(
                          ' *',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidth / 24,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'SIGN UP',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: screenWidth / 22,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Icon(
                        Icons.forward,
                        color: Colors.white,
                      )
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                      minimumSize: const Size.fromHeight(40)),
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Already have an account?'),
                    SizedBox(
                      width: 5,
                    ),
                    GestureDetector(
                      onTap: (() {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignInPage()),
                            (route) => false);
                      }),
                      child: Text(
                        'Sign In',
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: screenWidth / 24,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
