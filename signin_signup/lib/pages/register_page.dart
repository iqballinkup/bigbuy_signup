import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:signin_signup/pages/bangladesh_selected_page.dart';
import 'package:signin_signup/pages/international_selected_page.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  int _selectedRadioButton = 0;
  bool isRemembered = false;
  var emailPhoneText = "Email";

  final PageController _controller = PageController();
  TextEditingController emailPhoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xffD4EAFF),
        body: Column(
          children: <Widget>[
            Center(
              child: Text(
                'Register',
                style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth / 15,
                    color: Colors.blue,
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Radio(
                  value: 0,
                  groupValue: _selectedRadioButton,
                  onChanged: (value) {
                    setState(() {
                      _selectedRadioButton = value!;
                    });
                    _controller.jumpToPage(value!);
                  },
                ),
                Text(
                  'Bangladesh',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                Radio(
                  value: 1,
                  groupValue: _selectedRadioButton,
                  onChanged: (value) {
                    setState(() {
                      _selectedRadioButton = value!;
                    });
                    _controller.jumpToPage(value!);
                  },
                ),
                Text(
                  'International',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                )
              ],
            ),
            Expanded(
              child: PageView(
                physics: const NeverScrollableScrollPhysics(),
                controller: _controller,
                children: <Widget>[
                  // First Page
                  BangladeshSelectedPage(),
                  // Second Page
                  InternationalSelectedPage()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
